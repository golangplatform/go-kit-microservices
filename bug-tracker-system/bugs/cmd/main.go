package main

import service "bug-tracker-system/bugs/cmd/service"

func main() {
	service.Run()
}
