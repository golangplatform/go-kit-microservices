# Bug Tracker System

We will create a fictional bug tracker system with help of few microservices:
Users
Bugs
Notificator
Some of them will be accessible with JSON over HTTP, and internal communication will be done with gRPC.

# Let’s create our services with commands:

kit new service users_ms
kit new service bugs_ms
kit new service notifications_ms

kit generate service users_ms --dmw
kit generate service bugs_ms --dmw
kit generate service notifications_ms --dmw
kit generate service notifications_ms -t grpc --dmw