module bug-tracker-system

go 1.16

require (
	github.com/dave/jennifer v1.4.1 // indirect
	github.com/devimteam/microgen v0.9.2 // indirect
	github.com/fatih/structtag v1.2.0 // indirect
	github.com/go-kit/kit v0.10.0 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/kujtimiihoxha/kit v0.1.1 // indirect
	github.com/vetcher/go-astra v1.2.0 // indirect
	golang.org/x/net v0.0.0-20210331212208-0fccb6fa2b5c // indirect
	golang.org/x/sys v0.0.0-20210331175145-43e1dd70ce54 // indirect
	google.golang.org/genproto v0.0.0-20210331142528-b7513248f0ba // indirect
	google.golang.org/grpc v1.36.1 // indirect
)
