# go-kit-microservices

Repository for Microservices with gokit.

# go-kit review
We should understand that go-kit is not a framework, it’s a toolkit for building microservices in Go, including packages and interfaces. It is similar to Java Spring Boot but smaller in scope.

# Let’s init our project.
There is a kitgen command line tool to generate a service from template which is not ready to be used yet.


# Prerequisites
GoKit Cli needs to be installed using go get and go install so Go is a requirement to be able to test your services gokit is needed.

To utilise generation of gRPC service code through kit generate service <SERVICE_NAME> -t grpc you will need to install the grpc prequisites.
https://github.com/protocolbuffers/protobuf/releases/tag/v3.15.6 -Download protoc compiler
https://grpc.io/docs/languages/go/quickstart/
https://grpc.io/docs/protoc-installation/

Add to environment variable - C:\Program Files (x86)\protoc-3.15.6-win64\bin
Check version - protoc --version  # Ensure compiler version is 3+

After that
go get -u google.golang.org/grpc
go get -u github.com/golang/protobuf/protoc-gen-go


# go-kit CLI
There is a separate package to create a service from template:
go get github.com/go-kit/kit
go get github.com/kujtimiihoxha/kit

# Let’s create our services with commands:

kit new service microservice-one
kit new service microservice-two
kit new service microservice-three

This will generate the initial folder structure and the service interface. The interface is empty by default, let’s define the functions in our interface. We need a function for User creation, let’s start with this.